package com.ftekhar.springJwt.entities;

public enum Role {
    USER,
    ADMIN
}
